import {get} from '@loopback/rest';


export class HolaController {
  @get('/hola')
  hola(): string {
    return "hola mundo ";
  }
}
