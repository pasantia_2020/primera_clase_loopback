import {get} from '@loopback/rest';


export class MyArrayController {
  @get('/myarray')
  array(): any[] {

    var fechas = [

      {name: 'mayo', value: 5},
      {name: 'nobiembre', value: 11},
      {name: 'diciembre', value: 12},
      {name: 'junio', value: 6},
      {name: 'julio', value: 7},
      {name: 'agosto', value: 8},
      {name: 'enero', value: 1},
      {name: 'febrero', value: 2},
      {name: 'septienbre', value: 9},
      {name: 'octubre', value: 10},
      {name: 'marzo', value: 3},
      {name: 'abril', value: 4},

    ];
    fechas.sort((prev, next) => {
      return prev.value - next.value;
    })
    return fechas;
  }
}

